package com.yunmaozj.bean;

import java.util.ArrayList;
import java.util.List;

public class Node {
    //名称
    private String name;
    //登记名称
    private String gradeName;
    //下级节点
    private List<Node> chiledNodes;

    public Node(String name,String gradeName){
        this.name = name;
        this.gradeName = gradeName;
        this.chiledNodes = new ArrayList<Node>();
    }

    public int side(){
        return chiledNodes.size();
    }

    public void add(Node node){
        this.chiledNodes.add(node);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }
}
