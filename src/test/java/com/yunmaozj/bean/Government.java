package com.yunmaozj.bean;

import com.goktech.commons.excel.Excel;

public class Government {

    @Excel(name = "常量类型")
    private String contentType;
    @Excel(name = "编码")
    private String code;
    @Excel(name = "值")
    private String value;
    @Excel(name = "政府官网")
    private String zhengFuGuanWang;
    @Excel(name = "发改委")
    private String faGaiWei;
    @Excel(name = "科工信局")
    private String keGongXinJu;
    @Excel(name = "科技局")
    private String keJiJu;
    @Excel(name = "招商局网址")
    private String zhaoShangWang;
    @Excel(name = "地方商会")
    private String diFangShangHui;
    @Excel(name = "投促局")
    private String touChuJu;
    @Excel(name = "说明")
    private String desc;

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getZhengFuGuanWang() {
        return zhengFuGuanWang;
    }

    public void setZhengFuGuanWang(String zhengFuGuanWang) {
        this.zhengFuGuanWang = zhengFuGuanWang;
    }

    public String getFaGaiWei() {
        return faGaiWei;
    }

    public void setFaGaiWei(String faGaiWei) {
        this.faGaiWei = faGaiWei;
    }

    public String getKeGongXinJu() {
        return keGongXinJu;
    }

    public void setKeGongXinJu(String keGongXinJu) {
        this.keGongXinJu = keGongXinJu;
    }

    public String getKeJiJu() {
        return keJiJu;
    }

    public void setKeJiJu(String keJiJu) {
        this.keJiJu = keJiJu;
    }

    public String getZhaoShangWang() {
        return zhaoShangWang;
    }

    public void setZhaoShangWang(String zhaoShangWang) {
        this.zhaoShangWang = zhaoShangWang;
    }

    public String getDiFangShangHui() {
        return diFangShangHui;
    }

    public void setDiFangShangHui(String diFangShangHui) {
        this.diFangShangHui = diFangShangHui;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "Government{" +
                "contentType='" + contentType + '\'' +
                ", code='" + code + '\'' +
                ", value='" + value + '\'' +
                ", zhengFuGuanWang='" + zhengFuGuanWang + '\'' +
                ", faGaiWei='" + faGaiWei + '\'' +
                ", keGongXinJu='" + keGongXinJu + '\'' +
                ", keJiJu='" + keJiJu + '\'' +
                ", zhaoShangWang='" + zhaoShangWang + '\'' +
                ", diFangShangHui='" + diFangShangHui + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }

	public String getTouChuJu() {
		return touChuJu;
	}

	public void setTouChuJu(String touChuJu) {
		this.touChuJu = touChuJu;
	}
}
