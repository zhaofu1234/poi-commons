/**
 * 
 */
package com.yunmaozj.bean;

import com.goktech.commons.excel.Excel;

/**
 * @author Administrator
 *
 */
public class Entity {

	@Excel(name="学号")
	private String a;
	@Excel(name="姓名")
	private String b;
	@Excel(name="党校分期")
	private String c;
	@Excel(name="党校分班")
	private String d;
	@Excel(name="学院")
	private String e;
	@Excel(name="班级")
	private String f;
	@Excel(name="章节课程进度")
	private String g;
	@Excel(name="综合测试成绩")
	private String h;
	@Excel(name="集中考试成绩")
	private String i;
	public String getA() {
		return a;
	}
	public void setA(String a) {
		this.a = a;
	}
	public String getB() {
		return b;
	}
	public void setB(String b) {
		this.b = b;
	}
	public String getC() {
		return c;
	}
	public void setC(String c) {
		this.c = c;
	}
	public String getD() {
		return d;
	}
	public void setD(String d) {
		this.d = d;
	}
	public String getE() {
		return e;
	}
	public void setE(String e) {
		this.e = e;
	}
	public String getF() {
		return f;
	}
	public void setF(String f) {
		this.f = f;
	}
	public String getG() {
		return g;
	}
	public void setG(String g) {
		this.g = g;
	}
	public String getH() {
		return h;
	}
	public void setH(String h) {
		this.h = h;
	}
	public String getI() {
		return i;
	}
	public void setI(String i) {
		this.i = i;
	}
	@Override
	public String toString() {
		return "Entity [a=" + a + ", b=" + b + ", c=" + c + ", d=" + d + ", e=" + e + ", f=" + f + ", g=" + g + ", h="
				+ h + ", i=" + i + "]";
	}
	
}
