/**
 * 
 */
package com.yunmaozj.utils;

import org.junit.Test;

import com.goktech.commons.db.SQL;
import com.goktech.commons.excel.utils.writer.ExcelExport;
import com.goktech.commons.excel.utils.writer.ExcelJdbcExport;

import jdk.nashorn.internal.ir.annotations.Ignore;

/**
 * @author Administrator
 *
 */
public class TestExcelExport {

	private final String driver = "com.mysql.jdbc.Driver";
	private final String url = "jdbc:mysql://mysql:3306/test?useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull&autoReconnect=true&rewriteBatchedStatements=TRUE&serverTimezone=GMT%2B8";
	private final String user = "***";
	private final String passwd = "***";
	@Test
	public void TestExport1() {
		new ExcelExport().addContent(new String[]{"值1","值2"}).write("E:\\1.xlsx");
	}
	@Ignore
	@Test
	public void testexport2() {
		SQL sql = new SQL("archives")
				.field("id", "主键")
				.field("company_name", "公司名称")
				.field("update_date", "更新时间")
				.field("status", "档案状态")
				.field("legal_representative", "法定代表人")
				.field("create_time", "成立时间")
				.field("industry", "所属行业")
				.field("registered_capital", "注册资本")
				.field("business_registration_no", "工商注册号")
				.field("registration_status", "等级转态")
				.field("enterprise_credit_code", "企业信用代码")
				.field("approval_date", "登记机关")
				.field("registration_authority", "营业期限")
				.field("term", "企业类型");
		new ExcelJdbcExport(driver, user, passwd, url).sql(sql).executeQuery().write("E:\\2.xlsx");;
	}
	
	//@Test
	public void testExport3() {
		new ExcelJdbcExport(driver, user, passwd, url).sql("select * from archives").executeQuery().write("E:\\2.xlsx");
	}
}
