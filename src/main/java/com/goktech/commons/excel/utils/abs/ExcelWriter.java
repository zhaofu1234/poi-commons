/**
 * 
 */
package com.goktech.commons.excel.utils.abs;

import java.io.OutputStream;
import java.util.Collection;

/**
 * @author Administrator
 *
 */
public interface ExcelWriter<T> {

	// 设置表头
	public T setTitle(Collection<String> collection);

	// 设置需要导出的数据说
	public T setContent(Collection<Collection<Object>> collection);

	/**
	 * 创建工作薄
	 * 
	 * @param sheetName
	 * @return
	 */
	public T createSheet(String sheetName);

	// 导出
	public void write(OutputStream os);

}
