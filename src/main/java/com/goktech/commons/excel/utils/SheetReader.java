package com.goktech.commons.excel.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.goktech.commons.excel.utils.abs.AbstractReader;

/**
 * 读取Sheet的类，但是仍需要子类实现parse这个方法。
 * @param <T>
 */
public abstract class SheetReader<T> extends AbstractReader<T> {

	public SheetReader(Class<?> clazz, InputStream is) {
		super(clazz, is);
		if(this.workbook == null){
			initWorkbook();
		}
	}

	/**
	 * 读取所有的sheet
	 * @return
	 */
	@Override
	public T run() {
		Iterator<Sheet> sheetIterator = this.workbook.sheetIterator();
		int sheetIndex = 0;
		while (sheetIterator.hasNext()) {
			sheetIterator.next();
			run(sheetIndex++);
		}
		return this.getSelf();
	}

	/**
	 * <p>读取指定位置的sheet</p>
	 * @param sheetIndex
	 * @return
	 */
	public T run(int sheetIndex) {
		List<List<String>> list = new ArrayList<>();
		Sheet sheet = this.workbook.getSheetAt(sheetIndex);
		Iterator<Row> iterable = sheet.rowIterator();
		while (iterable.hasNext()) {
			Row row = iterable.next();
			List<String> celllist = new ArrayList<String>();
			int lastCell = row.getLastCellNum();
			for (int i = 0; i < lastCell; i++) {
				Cell cell = row.getCell(i);
				if (cell != null) {
					celllist.add(cell.getStringCellValue());
				} else {
					celllist.add(null);
				}
			}
			list.add(celllist);
		}
		parser(list);
		return this.getSelf();
	}

	public abstract void parser(List<List<String>> list);
}
