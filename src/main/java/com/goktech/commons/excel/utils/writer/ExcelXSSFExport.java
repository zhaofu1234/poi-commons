package com.goktech.commons.excel.utils.writer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.goktech.commons.excel.ExcelStyleResolver;
import com.goktech.commons.excel.RowFactory;
import com.goktech.commons.excel.Title;
import com.goktech.commons.excel.utils.abs.AbstractExcelWriter;
import com.goktech.commons.utils.StringUtils;

/**
 * @author zhongmh
 * @dec 普通导出，并不具备海量数据导出
 */
public class ExcelXSSFExport extends AbstractExcelWriter<ExcelXSSFExport> {

	protected XSSFWorkbook workbook;

	private RowFactory factory;

	public ExcelXSSFExport(Class<?> clazz) {
		super(clazz);
		this.workbook = new XSSFWorkbook();
	}
	
	public ExcelXSSFExport(Class<?> clazz, Map<String, Object> data) {
		this(clazz);
		this.data = data;
	}

	@Override
	public void write(OutputStream outputStream) throws IOException {
		this.workbook.write(outputStream);
	}
	
	public void write(File file) throws FileNotFoundException, IOException {
		this.write(new FileOutputStream(file));
	}
	@Override
	public ExcelXSSFExport initRows() {
		factory = new RowFactory(this.getLastRowsList(), sheet, workbook, this.data);
		if (excelStyle != null) {
			factory.setExcelStyle(excelStyle);
		}
		factory.create();
		this.rowIndex = factory.getRowIndex();
		if (logger.isDebugEnabled())
			logger.debug("initRows success end rowIndex -> {}", this.rowIndex);
		return this.getSelf();
	}

	@Override
	public ExcelXSSFExport initTitle() {
		if (this.paramsList.size() == 0) {
			getProperty();
		}
		String title = null;
		XSSFRow row = (XSSFRow) createRow(rowIndex);
		XSSFCellStyle cellStyle = workbook.createCellStyle();
		row.setRowStyle(cellStyle);
		for (int i = 0; i < this.paramsList.size(); i++) {
			XSSFCell cell = (XSSFCell) createCell(row, i);
			String value = null;
			if (this.excelMap.get(this.paramsList.get(i)) != null) {
				title = this.excelMap.get(paramsList.get(i)).name();
				value = StringUtils.isEmpty(title) ? this.paramsList.get(i) : title;
			} else {
				title = this.titleMap.get(this.paramsList.get(i)) == null ? this.paramsList.get(i)
						: this.titleMap.get(this.paramsList.get(i)).getFieldvalue();
				value = StringUtils.isEmpty(title) ? this.paramsList.get(i) : title;
			}
			cell.setCellValue(value);
			if (excelStyle != null )
			{
				if(this.titleMap.get(this.paramsList.get(i)) != null){
					cell.setCellStyle(excelStyle.getTitleStyle(this.titleFielName));
				}else{
					cell.setCellStyle(excelStyle.getTitleStyle(this.paramsList.get(i)));
				}
			}//判断表头是自增表头
				
		}
		if (logger.isDebugEnabled())
			logger.debug("title init success size:{}", this.paramsList.size());
		return this.getSelf();
	}
	
	@Override
	public ExcelXSSFExport createSheet(String name) {
		if (StringUtils.isEmpty(name)) {
			name = "表格";
		}
		this.sheet = this.workbook.createSheet(name);
		if (logger.isDebugEnabled())
			logger.debug("create sheetname:{} success", name);
		return this.getSelf();
	}

	public void setRowIndex(int idnex) {
		this.rowIndex = idnex;
	}

	@Override
	public ExcelXSSFExport getSelf() {
		return this;
	}

	@Override
	public ExcelXSSFExport initStyle() {
		excelStyle = new ExcelStyleResolver(clazz, workbook);
		return this.getSelf();
	}
	
	/**
	 * 对外开放工作薄对象
	 */
	@Override
	public Workbook workbook() {
		return this.workbook;
	}
	
	/**
	 * 开放工作薄对象 
	 * @return
	 */
	public Sheet sheet(){
		return sheet;
	}
	
	public Row createRow() {
		return super.createRow(rowIndex);
	}
	
	public ExcelXSSFExport createSheet(String name,int i){
		return this.getSelf();
	}
	
	//初始化自定义表头的方法
	public ExcelXSSFExport initializationTitle(){
		if(titleMap != null)
		this.titleMap.clear();//初始清除自定义表头
		if(paramsList != null)
		this.paramsList.clear();
		if(excelMap != null)
		this.excelMap.clear();
		this.getProperty();//重新添加模板的注解
		return this.getSelf();
	}
	
	public ExcelXSSFExport initializationTitle(int index, List<Title> titles){
		this.initializationTitle();
		this.addTitle(index, titles);
		return this.getSelf();
	}
	
}
