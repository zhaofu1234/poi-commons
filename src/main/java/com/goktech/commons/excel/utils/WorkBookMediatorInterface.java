/**
 * 
 */
package com.goktech.commons.excel.utils;

import java.util.Date;

import org.apache.poi.ss.usermodel.CellStyle;

import com.goktech.commons.excel.utils.abs.StyleMediator;

/**
 * @author zhongmh
 * @email yunmaozj@163.com
 * @createTime 2018年1月4日下午7:27:59
 * @desc
 */
public interface WorkBookMediatorInterface<T> {

	/**
	 * 创建行
	 * 
	 * @return
	 */
	public T row();

	/**
	 * 创建行
	 * 
	 * @return
	 */
	public T row(int indexRow);

	/**
	 * 创建单元格
	 * 
	 * @return
	 */
	public T cell();

	public T cell(int index);

	/**
	 * 设置样式
	 * 
	 * @return
	 */
	public T tyle(CellStyle cellStyle);
	
	public T style(String id);

	/**
	 * <p>
	 * 以当前单元格为中心点，宽度和高度会比实际上+1
	 * </p>
	 * 
	 * @param width
	 * @param height
	 * @return
	 */
	public T cellRangeAddress(int width, int height);

	/**
	 * <p>
	 * 合并单元格
	 * </p>
	 * 
	 * @param firstRow
	 * @param lastRow
	 * @param firstCell
	 * @param lastCell
	 * @return
	 */
	public T cellRangeAddress(int firstRow, int lastRow, int firstCell, int lastCell);

	/**
	 * 设置合并单元格的样式
	 * 
	 * @return
	 */
	public T cellRangeAddressStype();
	
	public T value(String vlaue);
	
	public T value(Date value);
	
	public T value(Double value);
	
	public T value(byte value);
	
	public T value(boolean value);
	
	/**
	 * <p>
	 * 更新索引,当手动填完之后需要手动更新索引以便确认自动填数据工厂知道起点
	 * </p>
	 * @param rowIndex
	 * @return
	 */
	public T updateIndex(int rowIndex);
	
	/**
	 * 设置excel样式对象
	 * @return
	 */
	public StyleMediator<T> style();
	
	public StyleMediator<T> style(CellStyle cellStyle);
	
	public T height(short height);
	
	public T heightInPoints(float heightInPoints);
	
	public T newSheet(String name);
	
	public T topBorderColor(int color);
	
	public T borderTop(int border);
	
	public T bottomBorderColor(int color);
	
	public T borderBottom(int border);
	
	public T rightBorderColor(int color);
	
	public T borderRight(int border);
	
	public T leftBorderColor(int color);
	
	public T borderLeft(int border);
	/**
	 * 设置合并单元格的默认样式
	 * @return
	 */
	public T defaultStyle();	
}
