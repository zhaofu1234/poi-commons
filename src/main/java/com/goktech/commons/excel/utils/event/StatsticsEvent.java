package com.goktech.commons.excel.utils.event;

import org.apache.poi.ss.usermodel.Cell;

//统计事件
public interface StatsticsEvent {

	/**
	 * 传入单元格对象，可以获取单元对象进行操作,实现者需要对不同的字段进行判断然后进行操作
	 * @param cell
	 */
	public void listener(Cell cell,String fieldName);
}
