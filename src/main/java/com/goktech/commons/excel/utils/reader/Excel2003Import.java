/**
 * 
 */
package com.goktech.commons.excel.utils.reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * @author Administrator
 *
 */
public class Excel2003Import extends ExcelImport{

	public Excel2003Import(Class<?> clazz, InputStream is) {
		super(clazz, is);
	}
	public Excel2003Import(Class<?> clazz, File file) throws FileNotFoundException {
		super(clazz, new FileInputStream(file));
	}
	public Excel2003Import(Class<?> clazz, String path) throws FileNotFoundException {
		this(clazz, new File(path));
	}
	protected Excel2003Import initWorkbook(){
		try {
			this.workbook = new HSSFWorkbook(is);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return this;
	}
	@Override
	public Excel2003Import getSelf() {
		return this;
	}

}
