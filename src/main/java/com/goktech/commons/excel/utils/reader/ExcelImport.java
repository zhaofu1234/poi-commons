/**
 * 
 */
package com.goktech.commons.excel.utils.reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import com.goktech.commons.excel.utils.SheetReader;

/**
 * @author Administrator
 *
 */
public class ExcelImport extends SheetReader<ExcelImport> {

	public ExcelImport(Class<?> clazz, InputStream is) {
		super(clazz, is);
	}

	public ExcelImport(Class<?> clazz, File file) throws FileNotFoundException {
		super(clazz, new FileInputStream(file));
	}

	public ExcelImport(Class<?> clazz, String path) throws FileNotFoundException {
		this(clazz, new File(path));
	}

	@Override
	public ExcelImport getSelf() {
		return this;
	}
	
	public void parser(List<List<String>> list) {
		for(int index = 0; index < list.size();index++) {
			if(index == 0) {
				initTitle(list.get(index));
			}else {
				putData(list.get(index));
			}
		}
	}

}
