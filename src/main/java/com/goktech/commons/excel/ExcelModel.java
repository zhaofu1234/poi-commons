/**
 * 
 */
package com.goktech.commons.excel;

/**
 * @author 24252
 * excel 二维模型
 */
public interface ExcelModel {

	public void add(int x, int y) throws ExcelException;
	
	public void add(int firstRow,int lastRow,int firstCol, int lastCol) throws ExcelException;
	
	public int getRowMax();
	
	public int getCellMax(int y) throws ExcelException;
	
	public boolean get(int x,int y)  throws ExcelException;
	
	/**
	 * 在一行的左边获取一个坐标点
	 * @return
	 */
	public int[] getLeftPoint() throws ExcelException;
	
	/**
	 * 在中间获取左边点
	 * @return
	 */
	public int[] getCenterPoint() throws ExcelException;
	
	/**
	 * 获取底部的坐标点
	 * @return
	 */
	public int[] getBottomPoint() throws ExcelException;
	
	public void print();
}
