/**
 * 
 */
package com.goktech.commons.excel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author 24252
 *
 */
public abstract class AbstractRow {

	private Logger logger = LoggerFactory.getLogger(AbstractRow.class);

	protected Rows rows;

	protected Sheet sheet;

	protected Workbook workbook;

	protected int maxIndex = 0;
	
	ExcelStyleResolver resolver;
	
	public AbstractRow( Rows rows, Sheet sheet, Workbook workbook) {
		super();
		this.rows = rows;
		this.sheet = sheet;
		this.workbook = workbook;
		//this.resolver = new ExcelStyleResolver(this.);
	}

	public org.apache.poi.ss.usermodel.Row createRow(int index) {
		org.apache.poi.ss.usermodel.Row row = this.sheet.createRow(index);
		return row;
	}

	public Cell createCell(org.apache.poi.ss.usermodel.Row row, int index) {
		Cell cell = row.createCell(index);
		if (logger.isDebugEnabled()) {
			logger.debug("自动行注解工程正在创建单元格的的坐标 row {} , cell {}", cell.getRowIndex(), cell.getColumnIndex());
		}
		return cell;
	}

	public abstract void create();

	public int getRowIndex() {
		return this.maxIndex;
	}

	public void setRowIndex(int index) {
		if (this.maxIndex <= index) {
			this.maxIndex = index + 1;
		}
	}
}
