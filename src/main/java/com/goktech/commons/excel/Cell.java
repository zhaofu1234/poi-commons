package com.goktech.commons.excel;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Cell {
	
	/**
	 * 表格样式
	 * @return
	 */
	Style style() default @Style;
	
	/**
	 * 表格的值
	 * @return
	 */
	String value();
	
	/**
	 * 在一行的索引位置
	 * @return
	 */
	String idnex() default "-1";
	
	/**
	 * 一行的宽度
	 * @return
	 */
	String width() default "-1";
	
	/**
	 * 高度的宽度
	 * @return
	 */
	String height() default "-1";
	
	String id() default  "";
}
