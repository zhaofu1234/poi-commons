/**
 * 
 */
package com.goktech.commons.field;

/**
 * @author 钟明宏
 *	字段校验工具的异常工具类
 */
public class ValiadationException extends IllegalArgumentException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4761419329593671382L;

	static{
		//避免类加载器死锁
		ValiadationException.class.getName();
	}
	
	public ValiadationException() {
		super();
	}



	public ValiadationException(String message, Throwable cause) {
		super(message, cause);
	}



	public ValiadationException(String s) {
		super(s);
	}



	public ValiadationException(Throwable cause) {
		super(cause);
	}

	@Override
	public String getMessage() {
		return super.getMessage();
	}
	
	
}
