package com.goktech.commons.utils;

import java.io.Serializable;
import java.util.HashMap;
/**
 * status 表示结果状态 0表示成功 -1.表示失败
 * code 请求状态
 * message 返回结果携带的信息
 * data 返回结果携带的数据
 * 返回给视图的结果对象
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class ResultUtil implements Serializable{
	
	private final static String MESSAGE = "message";
	private final static String CODE = "code";
	private final static String SUCCESS_CODE = "0";
	private final static String FAIL_CODE = "-1";
	private final static String CODETENT = "content";
	
	public static ResultMapHashMap<String, Object> initMap(){
		ResultMapHashMap<String, Object> resultMap = new ResultMapHashMap<>();
		return resultMap;
	}
	public static class ResultMapHashMap<K, V> extends HashMap<K, V> implements Serializable{

		public ResultMapHashMap<K,V> puts(K key, V value) {
			 super.put(key, value);
			 return this;
		}
	}
	public static ResultMapHashMap<String, Object> SUCCESS(){
		return SUCCESS(initMap());
	}
	
	private static ResultMapHashMap<String,Object> SUCCESS(ResultMapHashMap<String, Object> map){
		map.puts(CODE, SUCCESS_CODE);
		return map;
	}
	
	private static ResultMapHashMap<String,Object> SUCCESS(String message,ResultMapHashMap<String, Object> map){
		map.put(MESSAGE, message);
		return SUCCESS(map);
	}
	
	public static ResultMapHashMap<String, Object> SUCCESS(String message,Object object){
		return SUCCESS(message,initMap().puts(CODETENT, object));
	}
	
	public static ResultMapHashMap<String, Object> SUCCESS(String message){
		return SUCCESS(message,initMap());
	}
	
	public static ResultMapHashMap<String, Object> ERROR(){
		return ERROR(initMap());
	}
	
	public static ResultMapHashMap<String,Object> ERROR(ResultMapHashMap<String, Object> map){
		map.put(CODE,FAIL_CODE);
		return map;
	}
	
	public static ResultMapHashMap<String, Object> ERROR(String message,Object object){
		return ERROR(message,initMap().puts(CODETENT, object));
	}
	
	private static ResultMapHashMap<String, Object> ERROR(String message,ResultMapHashMap<String, Object> map){
		map.put(MESSAGE, message);
		return ERROR(map);
	}
	
	public static ResultMapHashMap<String, Object> ERROR(String message){
		return ERROR(message,initMap());
	}
	
	//将异常信息返回
	public static ResultMapHashMap<String, Object> returnExceptionMessage(Exception e){
		return ERROR(e.getMessage());
	}
	
	
}
