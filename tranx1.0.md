新增类：
1. [ExcelExport](src%2Fmain%2Fjava%2Fcom%2Fgoktech%2Fcommons%2Fexcel%2Futils%2Fwriter%2FExcelExport.java)

使用简单示范：

```
	@Test
	public void TestExport1() {
		new ExcelExport().addTitle(new String[]{"表头1","表头2"}).addContent(new String[]{"值1","值2"}).wirte("E:\\1.xlsx");
	}
```
2. [ExcelJdbcExport](src%2Fmain%2Fjava%2Fcom%2Fgoktech%2Fcommons%2Fexcel%2Futils%2Fwriter%2FExcelJdbcExport.java)
使用示范：
```
	@Ignore
	@Test
	public void testexport2() {
		SQL sql = new SQL("archives")
				.field("id", "主键")
				.field("company_name", "公司名称")
				.field("update_date", "更新时间")
				.field("status", "档案状态")
				.field("legal_representative", "法定代表人")
				.field("create_time", "成立时间")
				.field("industry", "所属行业")
				.field("registered_capital", "注册资本")
				.field("business_registration_no", "工商注册号")
				.field("registration_status", "等级转态")
				.field("enterprise_credit_code", "企业信用代码")
				.field("approval_date", "登记机关")
				.field("registration_authority", "营业期限")
				.field("term", "企业类型");
		new ExcelJdbcExport(driver, user, passwd, url).sql(sql).executeQuery().write("E:\\2.xlsx");;
	}
	@Test
	public void testExport3() {
		new ExcelJdbcExport(driver, user, passwd, url).sql("select * from archives").executeQuery().write("E:\\2.xlsx");
	}
```

3. ExcelImport

导出的类。比原来的代码更精简，重用性高
```
	@Test
	public void testImport() throws FileNotFoundException {
		List<Government> list = (List<Government>) new ExcelImport(Government.class, path).run().getList();
	}
```

4. 1.5.9 版本升级说明

该版本增加了对2003版xls表格解析，类名教Excel2003Import,没有扩展海量数据导入，考虑到使用的人不多，同时对上一个版本遗留的bug进行修改。
下一本升级计划:

5. 1.6.0 版本升级说明

该版本对导入功能进行了增加，支持选定 工作薄,以及默认所有的工作薄。